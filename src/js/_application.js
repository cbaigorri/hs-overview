$(function() {
  // measuring carousel
  $('.measuring .carousel').slick({
    dots: true,
    fade: true,
    arrows: true,
    cssEase: 'linear',
    prevArrow: '<button type="button" class="carousel-prev"><img src="assets/images/svg/arrow-prev.svg" title="Previous"></button>',
    nextArrow: '<button type="button" class="carousel-next"><img src="assets/images/svg/arrow-next.svg" title="Next"></button>',
  });

  // how we are doing
  $('.how-we-are-doing .carousel').slick({
    dots: true,
    fade: true,
    arrows: true,
    cssEase: 'linear',
    prevArrow: '<button type="button" class="carousel-prev"><img src="assets/images/svg/arrow-prev-alt.svg" title="Previous"></button>',
    nextArrow: '<button type="button" class="carousel-next"><img src="assets/images/svg/arrow-next-alt.svg" title="Next"></button>',
  });

  // learn more
  $("#learn-more").click(function(e) {
    e.preventDefault();
    $('html, body').animate({
      scrollTop: $("#how-we-are-doing").offset().top
    }, 1000);
  });

  // parallax
  var $el = $('.parallax-background');
  $(window).on('scroll', function() {
    $el.each(function(index) {
      var translateValue = (window.scrollY - $(this).offset().top) / 4;
      if (translateValue < 0) {
        translateValue = 0;
      }
      $(this).css({
        'transform':'translate3d(0px,' + translateValue + 'px, 0px)'
      });
    });
  });
});
