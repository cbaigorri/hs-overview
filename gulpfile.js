// Gulp Plugin Setup

var gulp = require('gulp');
var browserSync = require('browser-sync').create();


var sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    uglify = require('gulp-uglify'),
    jshint = require('gulp-jshint'),
    header  = require('gulp-header'),
    rename = require('gulp-rename'),
    minifyCSS = require('gulp-clean-css'),
    notify = require("gulp-notify"),
    cache = require('gulp-cache'),
    replace = require('gulp-replace'),
    plumber = require('gulp-plumber'),
    gutil = require('gulp-util'),
    concat = require('gulp-concat'),
    sourcemaps = require ('gulp-sourcemaps'),
    package = require('./package.json');


// Project Variables

_local_server = 'handprinting-overview.dev';


// Handle Errors

var onError = function(err) {
  gutil.beep();
  gutil.log( 'An error occurred:', err.message );
  notify.onError({
    title:    "Gulp",
    subtitle: "Failure!",
    message:  "Woops!",
    sound:    "Beep"
  })(err);
  this.emit('end');
 };


// Concatinate and Minify CSS, Compile SASS

// Compile Our Sass
gulp.task('css', function () {
  return gulp.src('src/scss/*.scss')
  .pipe(plumber({errorHandler: onError}))
  .pipe(sass({outputStyle: 'expanded'}))
  .pipe(autoprefixer({
    browsers: ['last 4 versions'],
    cascade: false
  }))
  .pipe(concat('main.min.css'))
  .pipe(gulp.dest('assets/css'))
  .pipe(notify("CSS Compiled"))
  .pipe(browserSync.stream());
});


// Concatinate and Minify JS

gulp.task('js', function(){
  return gulp.src([
    'src/js/_jquery-2.2.4.min.js',
    'src/js/slick.min.js',
    'src/js/_application.js'
  ])
  //.pipe(jshint('.jshintrc'))
  //.pipe(jshint.reporter('default'))
  //.pipe(header(banner, { package : package }))
  // .pipe(gulp.dest('assets/js'))
  .pipe(uglify())
  //.pipe(header(banner, { package : package }))
  .pipe(concat('main.js'))
  .pipe(rename({ suffix: '.min' }))
  .pipe(gulp.dest('assets/js'))
  .pipe(notify({
    title: 'Gulp',
    subtitle: 'success',
    message: 'JS Compiled'
  }))
  // .pipe(browserSync.reload({stream:true, once: true}));
});



// Setup Browser sync

gulp.task('browser-sync', function() {
  // browserSync.init({
  //   proxy: _local_server
  // });
  browserSync.init({
    server: {
        baseDir: "./"
    }
  });
});

gulp.task('bs-reload', function () {
  browserSync.reload();
});


// Gulp Tasks

gulp.task('default', ['css', 'js', 'bs-reload', 'browser-sync'], function () {
  gulp.watch("src/scss/main.scss", ['css']);
  gulp.watch("src/scss/*/*.scss", ['css']);
  gulp.watch("src/js/*.js", ['js']);
});
